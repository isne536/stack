#include <iostream>
#include <stack>
using namespace std;

stack<int> operator + (stack<int>, stack<int>);
stack<int> operator - (stack<int>, stack<int>);

int main()
{
	stack<int> stack1,stack2; //variable to store value in stack form
	stack<int> sum;
	stack<int> different;
	
	int set1[19] = {1,3,4,4,8,2,3,5,0,8,9,7,4,5,3,4,5,2,3 }; //first array
	int set2[17] = {2,3,4,7,2,3,4,7,0,9,4,7,3,0,4,7,5}; //second array
	
	for(int i=0;i<19;i++)
	{
		stack1.push(set1[i]); //push value from set1 array to stack1
	}

	for(int i=0;i<17;i++)
	{
		stack2.push(set2[i]); //push value from set2 array to stack2
	}
	
	cout << "Sum is ";
	sum = stack1 + stack2;
	for (stack <int> i = sum; !i.empty(); i.pop()) //loop to print result on display
	{
		cout << i.top();
	}
	cout << endl;
	
	cout << "Different is ";
	different = stack1 - stack2;
	for (stack <int> i = different; !i.empty(); i.pop()) //loop to print result on display
	{
		cout << i.top();
	}
	cout << endl;
	system("pause");
	return 0;
}

stack<int> operator + (stack<int> stack1, stack<int> stack2) //loop to calculate
{
	stack<int>num;
	int tmp1, tmp2;
	while (!stack1.empty()&&!stack2.empty())
	{
		tmp1 = stack1.top() + stack2.top(); //variable to store result
		tmp2 = tmp1;
		stack1.pop(); //remove value at the top of stack1
		stack2.pop(); //remove value at the top of stack2
		if (tmp1 >= 10) 
		{
			tmp1 = tmp1 % 10;
			if (!stack1.empty()) 
			{
				stack1.top() = stack1.top() + 1;
			}
			else 
			{
				tmp1 = tmp2;
			}		
		}
		num.push(tmp1);
	}
	if (!stack1.empty()) 
	{
		while (!stack1.empty()) 
		{
			tmp1 = stack1.top();
			stack1.pop();
			num.push(tmp1);
		}
	}
	else if (!stack2.empty()) 
	{
		while (!stack2.empty()) 
		{
			tmp1 = stack2.top();
			stack2.pop();
			num.push(tmp1);
		}
	}
	return num;
}

stack<int> operator - (stack<int> stack1, stack<int> stack2) //loop to calculate
{
	stack<int>num;
	int tmp;
	if (stack1.size() > stack2.size()) 
	{
		while (!stack1.empty() && !stack2.empty()) 
		{
			tmp = stack1.top() - stack2.top(); //variable to store result
			stack1.pop(); //remove value at the top of stack1
			stack2.pop(); //remove value at the top of stack2
			if (tmp < 0) 
			{
				tmp = tmp + 10;
				if (!stack1.empty()) 
				{
					stack1.top() = stack1.top() - 1;
				}
			}
			num.push(tmp);
		}
		if (!stack1.empty()) 
		{
			while (!stack1.empty()) 
			{
				tmp = stack1.top();
				stack1.pop();
				num.push(tmp);
			}

		}
	}
	else if (stack1.size() < stack2.size()) 
	{
		while (!stack1.empty() && !stack2.empty()) 
		{
			tmp = stack2.top() - stack1.top();
			stack2.pop();
			stack1.pop();
			if (tmp < 0) 
			{
				tmp = tmp + 10;
				if (!stack1.empty()) 
				{
					stack2.top() = stack2.top() - 1;
				}
			}
			num.push(tmp);
		}
		if (!stack2.empty()) 
		{
			while (!stack2.empty()) 
			{
				tmp = stack2.top();
				stack2.pop();
				if (stack2.empty()) 
				{
					tmp = tmp * -1;
				}
				num.push(tmp);
			}
		}
	}
		else if (stack1.size() == stack2.size()) 
		{
			if (stack1 > stack2) 
			{
				while (!stack1.empty() && !stack2.empty()) 
				{
							tmp = stack2.top() - stack1.top();
							stack2.pop();
							stack1.pop();
							if (tmp < 0) 
							{
								tmp = tmp + 10;
								if (!stack1.empty()) 
								{
									stack2.top() = stack2.top() - 1;
								}
							}
							num.push(tmp);
				}
				if (!stack1.empty()) 
				{
					while (!stack1.empty()) 
					{
						tmp = stack1.top();
						stack1.pop();
						num.push(tmp);
					}
				}
			}
		else if (stack2 > stack1) 
		{
			while (!stack1.empty() && !stack2.empty()) 
			{
				tmp = stack2.top() - stack1.top();
				stack2.pop();
				stack1.pop();
				if (tmp < 0) 
				{
					tmp = tmp + 10;
					if (!stack1.empty()) 
					{
						stack2.top() = stack2.top() - 1;
					}
				}
				num.push(tmp);
			}
		}
	}
	return num;
}

